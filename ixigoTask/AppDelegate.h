//
//  AppDelegate.h
//  ixigoTask
//
//  Created by Munish Sehdev on 16/02/16.
//  Copyright © 2016 munish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

