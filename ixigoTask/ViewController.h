//
//  ViewController.h
//  ixigoTask
//
//  Created by Munish Sehdev on 16/02/16.
//  Copyright © 2016 munish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate,UITableViewDataSource, UIActionSheetDelegate>
{
    NSArray *flightsData;
    CGSize screenSize;
    NSDictionary *responseJson;
    UITableView* resultTbl ;
}


@end

