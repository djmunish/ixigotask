//
//  NetworkHandler.h
//  iXigoPlayer
//
//  Created by Munish Sehdev on 11/11/15.
//  Copyright © 2015 iXigo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkHandler : NSObject 

+ (NetworkHandler *)sharedInstance;
-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure;


@end
