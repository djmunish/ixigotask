//
//  NetworkHandler.m
//  iXigo
//
//  Created by Munish Sehdev on 11/11/15.
//  Copyright © 2015 iXigo. All rights reserved.
//

#import "NetworkHandler.h"
#import "InternetCheck.h"
@implementation NetworkHandler

static NetworkHandler *shared=NULL;

+ (NetworkHandler *)sharedInstance {
    if (nil != shared)  {
        return shared;
    }
    static dispatch_once_t pred;        // Lock
    dispatch_once(&pred, ^{             // This code is called at most once per app
        shared = [[NetworkHandler alloc] init];
    });
    
    return shared;
}

- (void)getJsonResponse:(NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    if ([self testInternetConnection]) {
        
        NSURLSession * session = [NSURLSession sharedSession];
        NSURL * url = [NSURL URLWithString: urlStr];
        
        // Asynchronously Api is hit here
        NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
            if (data) {
                NSDictionary * json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                NSLog(@"%@",json);
                success(json);
            } else {
                NSLog(@"Failed to fetch %@: %@", url, error);
                failure(error);
            }
        }];
        
        [dataTask resume] ;

    }else{
        NSError *error = nil;
        failure(error);

        NSLog(@"NO NOT WORKING");
    }
   
}

- (BOOL)testInternetConnection {
    InternetCheck *reach = [InternetCheck reachabilityForInternetConnection];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    if (netStatus == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}




@end
