//
//  ViewController.m
//  ixigoTask
//
//  Created by Munish Sehdev on 16/02/16.
//  Copyright © 2016 munish. All rights reserved.
//

#import "ViewController.h"
#import "NetworkHandler.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    screenSize =[UIScreen mainScreen].bounds.size;
    
    
    UIActivityIndicatorView* spinnerComplete = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinnerComplete.frame = CGRectMake((screenSize.width -24)/2.0, (screenSize.height - 24 )/2.0, 24, 24);
    [spinnerComplete setColor:[UIColor grayColor]];
    [self.view addSubview:spinnerComplete];
    [spinnerComplete startAnimating];
    
    [[NetworkHandler sharedInstance] getJsonResponse:[NSString stringWithFormat:@"http://blog.ixigo.com/sampleflightdata.json"] success:^(NSDictionary *responseDict)
     {
         NSLog(@"response ===>%@",responseDict);
         flightsData = [responseDict valueForKey:@"flightsData"];
         responseJson = responseDict;
         [self resultUIupdate:spinnerComplete];
         
     } failure:^(NSError *error) {
         // error handling here ...
         NSLog(@"Error Occured while fetching data===>%@",error.description);
     }];
    
    
    
}

- (void)sorting:(NSDictionary*)publicTimeline{


    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:publicTimeline];
    [dict keysSortedByValueUsingComparator:^(id obj1, id obj2) {
        return (NSComparisonResult)[obj1 compare:obj2];
    }];
}

- (void)resultUIupdate:(UIActivityIndicatorView*)activityIndicator{

    [activityIndicator stopAnimating];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [activityIndicator removeFromSuperview];
        
        UILabel* flightRouteLbl =[[UILabel alloc] init];
        [flightRouteLbl setFrame:CGRectMake(0, 20, (screenSize.width - 0), 44)];
        [flightRouteLbl setTextColor:[UIColor lightGrayColor]];
        flightRouteLbl.textAlignment = NSTextAlignmentCenter;
        flightRouteLbl.font = [UIFont systemFontOfSize:36];
        [self.view addSubview:flightRouteLbl];
        flightRouteLbl.text=@"DEL -> MUM" ;

        
        resultTbl= [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(flightRouteLbl.frame)  , screenSize.width, screenSize.height - CGRectGetMaxY(flightRouteLbl.frame))];
        resultTbl.dataSource=self;
        resultTbl.delegate=self;
        [self.view addSubview:resultTbl];

        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(screenSize.width-70, 20, 60, 40)];
        button.backgroundColor = [UIColor grayColor];
        button.clipsToBounds = YES;
        button.layer.cornerRadius = 0;
        [button setTitle:@"Sort" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
        button.exclusiveTouch=YES;
        [button addTarget:self action:@selector(sortBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
    });
}


#pragma mark - Actionsheet
- (void)sortBtnAction:(UIButton*)sender{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Sort in Ascending Order:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Sort by Landing Time",
                            @"Sort by Take Off Time",
                            @"Sort by Price",
                            nil];
    popup.tag = 1;
    [popup showInView:self.view];
}



- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self sortDictionOnkey:@"landingTime"];
            break;
        case 1:
            [self sortDictionOnkey:@"takeoffTime"];
            break;
        case 2:
            [self sortbyPrice];
            break;
        default:
            break;
    }
    
}

#pragma mark - UITableView DataSources


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [flightsData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 180;
}


#pragma mark - UITableView Delegates
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    } else {
        
        for(UIView *view in cell.contentView.subviews)
        {
            [view removeFromSuperview];
        }
    }
    
    UILabel *flightTitleLabel=[self createLabel:CGRectMake(10, 5, 150, 20) text:[NSString stringWithFormat:@"Class: %@",[[flightsData valueForKey:@"class"] objectAtIndex:indexPath.row]]];
    [cell.contentView addSubview:flightTitleLabel];

    
    UILabel *flightRouteLabel=[self createLabel:CGRectMake(10, CGRectGetMaxY(flightTitleLabel.frame)+5, 250, 20) text:[NSString stringWithFormat:@"%@ -> %@",[self fullStringName:[[flightsData valueForKey:@"originCode"] objectAtIndex:indexPath.row] airportOrairline:YES],[self fullStringName:[[flightsData valueForKey:@"destinationCode"] objectAtIndex:indexPath.row] airportOrairline:YES]]];
    [cell.contentView addSubview:flightRouteLabel];
    

    UILabel *flightNameLabel=[self createLabel:CGRectMake(10, CGRectGetMaxY(flightRouteLabel.frame)+5, 250, 20) text:[NSString stringWithFormat:@"Flight: %@",[self fullStringName:[[flightsData valueForKey:@"airlineCode"] objectAtIndex:indexPath.row] airportOrairline:NO]]];
    [cell.contentView addSubview:flightNameLabel];

    
    UILabel *durationLbl=[self createLabel:CGRectMake(10, CGRectGetMaxY(flightNameLabel.frame)+5, 250, 20) text:[NSString stringWithFormat:@"Duration: %@",[self timeDuration:[[[flightsData valueForKey:@"landingTime"] objectAtIndex:indexPath.row] doubleValue] departure:[[[flightsData valueForKey:@"takeoffTime"] objectAtIndex:indexPath.row] doubleValue]]]];
    [cell.contentView addSubview:durationLbl];

    
    UILabel *takeOffTimeLabel=[self createLabel:CGRectMake(10, CGRectGetMaxY(durationLbl.frame)+5, 300, 20) text:[NSString stringWithFormat:@"TakeOff time: %@",[self flightTimings:[[[flightsData valueForKey:@"takeoffTime"] objectAtIndex:indexPath.row] longLongValue]/1000]]];
    [cell.contentView addSubview:takeOffTimeLabel];

    
    
    UILabel *landingTimeLabel=[self createLabel:CGRectMake(10, CGRectGetMaxY(takeOffTimeLabel.frame)+5, 300, 20) text:[NSString stringWithFormat:@"Landing Time: %@",[self flightTimings:[[[flightsData valueForKey:@"landingTime"] objectAtIndex:indexPath.row] longLongValue]/1000]]];
    [cell.contentView addSubview:landingTimeLabel];
    

    UILabel *priceLabel=[self createLabel:CGRectMake(screenSize.width - 130, 5, 120, 20) text:[NSString stringWithFormat:@"Price: ₹%@",[[flightsData valueForKey:@"price"] objectAtIndex:indexPath.row]]];
    [cell.contentView addSubview:priceLabel];
    priceLabel.textAlignment=NSTextAlignmentRight;
    
    return cell;
    
}


#pragma mark - UILabel
- (UILabel *)createLabel:(CGRect)frame text:(NSString*)text{

    UILabel *Label=[[UILabel alloc] init];
    [Label setFrame:frame];
    [Label setTextColor:[UIColor lightGrayColor]];
    Label.textAlignment=NSTextAlignmentLeft;
    Label.text=text;
    
    return Label;
}

#pragma mark - Full Names
- (NSString*)fullStringName:(NSString*)str airportOrairline:(BOOL)airportMap{
    if(airportMap)
        return [[responseJson valueForKey:@"airportMap"] valueForKey:str];
    else
        return [[responseJson valueForKey:@"airlineMap"] valueForKey:str];


}

#pragma mark - Flight Durations
- (NSString *)timeDuration:(long)arrivalTime departure:(long)departureTime{
    long arrivedSeconds = ( arrivalTime - departureTime )/1000;
    return [NSString stringWithFormat:@"%02lu:%02lu:%02lu",(unsigned long) arrivedSeconds / 3600,(unsigned long)(arrivedSeconds % 3600) / 60, (unsigned long)(arrivedSeconds %3600) % 60];
}

#pragma mark - Flight Timings

- (NSString *)flightTimings:(long)seconds {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:seconds];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d. MMM YY HH:MM:SS"];
    
    return [formatter stringFromDate:date];
}


# pragma mark - Sorting Dictionary

- (void)sortDictionOnkey:(NSString*)key{
    NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:YES];
    flightsData = [flightsData sortedArrayUsingDescriptors:[NSArray arrayWithObject:brandDescriptor]];
    [resultTbl reloadData];
}

- (void)sortbyPrice{
    
    NSMutableArray *sortingFligthForPrices = [[NSMutableArray alloc] init] ;

    for (int i =0; i<[[flightsData valueForKey:@"price"] count]; i++) {

        [sortingFligthForPrices addObject:@{@"airlineCode":[[flightsData valueForKey:@"airlineCode"] objectAtIndex:i],
                         @"class":[[flightsData valueForKey:@"class"] objectAtIndex:i],
                         @"destinationCode":[[flightsData valueForKey:@"destinationCode"] objectAtIndex:i],
                         @"landingTime":[[flightsData valueForKey:@"landingTime"] objectAtIndex:i],
                         @"originCode":[[flightsData valueForKey:@"originCode"] objectAtIndex:i],
                         @"takeoffTime":[[flightsData valueForKey:@"takeoffTime"] objectAtIndex:i],
                         @"price":[NSNumber numberWithInteger:[[[flightsData valueForKey:@"price"] objectAtIndex:i] integerValue]],
                         }];
    }
    
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"price" ascending: YES];
    
    flightsData = [sortingFligthForPrices sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
    [resultTbl reloadData];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
